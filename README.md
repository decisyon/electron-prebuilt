[![DcyLogo](http://support.decisyon.com/tomcat/images/logoSmall.png)](http://http://www.decisyon.com/)

## Repository informations

*This is a public repository of Decisyon because this repository must be accessible from different sources through npm install.* 

# Decisyon electron-prebuilt

Install Electron prebuilt binaries for export pdf. 

[Electron](http://electron.atom.io) is a JavaScript runtime that bundles Node.js
and Chromium. You use it similar to the `node` command on the command line for
executing JavaScript programs. 


## Installation

Download and install the latest build of Electron for your OS and add it to your
project's `package.json` or by command shell:

```shell
npm install https://git@bitbucket.org/decisyon/electron-prebuilt.git --save-dev
```

```json
"dependencies" : {
	"electron-prebuild": "https://git@bitbucket.org/decisyon/electron-prebuilt.git"
}
```

## About Decisyon electron-prebuilt

This is a fork of the official [Electron-prebuild repository](https://github.com/electron-userland/electron-prebuilt). 
Substantial changes have been made to this repository to cover the needs of Decisyon's EXPORT PDF.
Now this fork automatically fetches the correct versions of electron for UNIX and Windows operating systems.

Works on Windows and Linux (Centos 6.5 - 7) that Electron supports (e.g. Electron
[does not support Windows XP](https://github.com/electron/electron/issues/691)).

**Note** This is a version 1.7.4 of original electron-prebuilt package https://github.com/electron-userland/electron-prebuilt.

## Usage

Mainly this package is used by nightmareJS for create the Decisyon EXPORT PDF.

## How to map this repository with synchronization with the original package 

* Clone the repository in your local machine:  
		> git clone https://git@bitbucket.org/decisyon/electron-prebuilt.git  
		> cd electron    

* Now add original Github repo as a new remote in Bitbucket called "sync":  
		> git remote add sync https://github.com/electron-userland/electron-prebuilt.git        
  
* Verify what are the remotes currently being setup for "electron".  
	This following command should show "fetch" and "push" for two remotes i.e. "origin" and "sync":  
		> git remote -v    

* Now do a pull from the "master" branch in the "sync" remote:  
		> git pull sync master    

* Setup a local branch called "github"track the "sync" remote's "master" branch:  
		> git branch --set-upstream-to origin/master master    

* Now push the local "master" branch to the "origin" remote in Bitbucket:  
		> git push -u origin master

