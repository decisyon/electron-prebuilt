#!/usr/bin/env node

// maintainer note - x.y.z-ab version in package.json -> x.y.z
const version 	= require('./package').versions.electron.replace(/-.*/, '');
const fs 		= require('fs');
const os		= require('os');
const path 		= require('path');
const extract 	= require('extract-zip');
const download 	= require('electron-download');

const dcyPlatformsToInstall = ['win32', 'linux'];
const platformArchitecture 	= 'x64';

//unzips and makes txt file point at the correct executable
extractFile = (err, zipPath, platformName, platformPath, folderDist, pathFileName) => {
  if (err) throw err;
  extract(zipPath, {dir: path.join(__dirname, folderDist)}, function (err) {
	if (err) throw err;
	fs.writeFile(path.join(__dirname, pathFileName), platformPath, function (err) {
	  if (err) throw err;
	})
  });
  console.log('DONE!The electron package version ['+version+'] is been installed for os '+platformName+'-'+platformArchitecture);
};

getDistPlatformPath = (platformName) => {
  let out = platformName+'-';
  
  switch (platformName) {
	  case 'linux':
		  out += 'dist/electron';
		  break;
	  case 'win32':
		  out += 'dist/electron.exe';
		  break;
  }
  
  return out;
};

installPlatform = (platformName, index) => {
	let folderDist   = platformName+'-dist';
	
	let installedVersion = null;
	try {
		installedVersion = fs.readFileSync(path.join(__dirname, folderDist, 'version'), 'utf-8').replace(/^v/, '')
	} catch (ignored) {
		// do nothing
	}
	
	let pathFileName = platformName + '-path.txt';
	
	let platformPath = getDistPlatformPath(platformName);
	
	if (installedVersion === version && fs.existsSync(path.join(__dirname, platformPath))) {
		console.warn('The electron package version ['+version+'] is already installed for os '+platformName+'-'+platformArchitecture);
	}
	
	// downloads if not cached
	download({
		cache: undefined,
		version: version,
		platform: platformName,
		arch: platformArchitecture,
		strictSSL: process.env.npm_config_strict_ssl === 'true',
		quiet: ['info', 'verbose', 'silly', 'http'].indexOf(process.env.npm_config_loglevel) === -1
	}, (err, zipPath) => { extractFile(err, zipPath, platformName, platformPath, folderDist, pathFileName); })
};

debugger;

setTimeout(() => { 
	debugger;
	if(process.env.npm_lifecycle_event == 'debug-install'){
		setTimeout(function() { 
			debugger;
			dcyPlatformsToInstall.forEach(installPlatform);
		}, 1000);
	}
	else{
		dcyPlatformsToInstall.forEach(installPlatform);
	}
}, 10000);
