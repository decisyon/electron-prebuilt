const fs = require('fs')
const path = require('path')

const os = require('os');

const platform = os.platform();
const pathFileName = platform + '-path.txt';
const pathFile = path.join(__dirname, pathFileName);

if (fs.existsSync(pathFile)) {
  module.exports = path.join(__dirname, fs.readFileSync(pathFile, 'utf-8'));
} else {
  throw new Error('Electron failed to install correctly, please delete node_modules/electron and try installing again')
}
